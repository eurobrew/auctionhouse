package com.mayne.util;

import java.math.BigDecimal;

import static java.math.MathContext.DECIMAL128;
import static java.math.RoundingMode.HALF_UP;

public abstract class PriceHelper {

    private static final Integer DEFAULT_SCALE = 2;

    public static BigDecimal price(String value) {
        return new BigDecimal(value, DECIMAL128).setScale(DEFAULT_SCALE, HALF_UP);
    }

    public static BigDecimal price(Integer value) {
        return new BigDecimal(value, DECIMAL128).setScale(DEFAULT_SCALE, HALF_UP);
    }
}

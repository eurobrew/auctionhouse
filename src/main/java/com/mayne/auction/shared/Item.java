package com.mayne.auction.shared;

import java.util.Objects;
import java.util.UUID;

public abstract class Item {

    protected final UUID identifier;

    protected Item(UUID identifier) {
        this.identifier = identifier;
    }

    public UUID identifier() {
        return identifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return Objects.equals(identifier, item.identifier);
    }

    @Override
    public int hashCode() {
        return identifier != null ? identifier.hashCode() : 0;
    }
}

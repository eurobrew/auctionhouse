package com.mayne.auction.shared;

import com.mayne.auction.shared.Item;

import java.math.BigDecimal;
import java.util.UUID;

public class AuctionItem extends Item {

    private final BigDecimal reserve;

    public AuctionItem(BigDecimal reserve) {
        super(UUID.randomUUID());
        this.reserve = reserve;
    }

    public BigDecimal reserve() {
        return reserve;
    }
}

package com.mayne.auction.shared;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.util.concurrent.AbstractIdleService;
import com.mayne.auction.participant.Bid;
import com.mayne.auction.participant.BidResult;

import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mayne.auction.participant.BidResult.*;

public class AuctionService extends AbstractIdleService {

    private static final Logger LOGGER = Logger.getLogger(AuctionService.class.getName());

    private final Map<UUID, AuctionItem> items = new HashMap<>();
    private final LinkedListMultimap<UUID, Bid> bids = LinkedListMultimap.create();
    private final Set<UUID> currentItems = new HashSet<>();

    @Override
    protected void startUp() throws Exception {
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Starting up auction service");
        }
    }

    @Override
    protected void shutDown() throws Exception {
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Shutting down auction service");
        }
        items.clear();
        bids.clear();
        currentItems.clear();
    }

    public void add(AuctionItem auctionItem) throws IllegalStateException {
        if (items.containsKey(auctionItem.identifier())) {
            throw new IllegalStateException("Cannot add the same item more than once");
        }
        items.put(auctionItem.identifier(), auctionItem);
    }

    public void start(AuctionItem auctionItem) throws IllegalStateException {
        if (!items.containsKey(auctionItem.identifier())) {
            throw new IllegalStateException("Unknown item");
        } else if (currentItems.contains(auctionItem.identifier())) {
            throw new IllegalStateException("Auction already started");
        } else if (bids.containsKey(auctionItem.identifier())) {
            AuctionResult auctionResult = determineAuctionResult(auctionItem);
            if (auctionResult.status().equals(AuctionStatus.SUCCESS)) {
                throw new IllegalStateException("Item has already been sold at auction");
            }
        }
        currentItems.add(auctionItem.identifier());
    }

    public BidResult bid(Item item, Bid bid) throws IllegalStateException {
        UUID identifier = item.identifier();
        if (!currentItems.contains(identifier)) {
            throw new IllegalStateException("Auction not currently running");
        }
        if (bids.containsKey(identifier)) {
            Bid highestBid = determineHighestBid(item);
            if (bid.amount().compareTo(highestBid.amount()) <= 0) {
                return BID_TOO_LOW;
            }
        }
        bids.put(identifier, bid);
        return meetsBidReserve(item, bid) ? SUCCESS : RESERVE_NOT_MET;
    }

    public AuctionResult end(AuctionItem auctionItem) {
        currentItems.remove(auctionItem.identifier());
        return determineAuctionResult(auctionItem);
    }

    public Optional<Bid> highestBid(AuctionItem auctionItem) {
        if (bids.containsKey(auctionItem.identifier())) {
            return Optional.of(determineHighestBid(auctionItem));
        }
        return Optional.empty();
    }

    public Optional<AuctionResult> result(Item item) {
        if (!items.containsKey(item.identifier())) {
            return Optional.empty();
        }
        return Optional.of(determineAuctionResult(items.get(item.identifier())));
    }

    public Collection<AuctionItem> items() {
        return items.values();
    }

    public Collection<UUID> currentItems() {
        return currentItems;
    }

    public Collection<Bid> bids() {
        return bids.values();
    }

    private Bid determineHighestBid(Item item) {
        List<Bid> currentBids = bids.get(item.identifier());
        Collections.sort(currentBids);
        Collections.reverse(currentBids);
        return currentBids.get(0);
    }

    private AuctionResult determineAuctionResult(AuctionItem auctionItem) {
        if (bids.containsKey(auctionItem.identifier())) {
            return new AuctionResult(auctionItem, determineHighestBid(auctionItem));
        } else {
            return new AuctionResult(auctionItem);
        }
    }

    private boolean meetsBidReserve(Item item, Bid bid) {
        BigDecimal reserve = items.get(item.identifier()).reserve();
        return bid.amount().compareTo(reserve) >= 0;
    }
}

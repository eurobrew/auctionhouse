package com.mayne.auction.shared;

import com.mayne.auction.participant.Bid;
import com.mayne.auction.participant.Participant;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

import static com.mayne.auction.shared.AuctionStatus.FAILURE;
import static com.mayne.auction.shared.AuctionStatus.SUCCESS;

public class AuctionResult {

    private final AuctionItem auctionItem;
    private final Optional<Bid> bid;

    public AuctionResult(AuctionItem auctionItem, Bid bid) {
        this.auctionItem = auctionItem;
        this.bid = Optional.of(bid);
    }

    public AuctionResult(AuctionItem auctionItem) {
        this.auctionItem = auctionItem;
        this.bid = Optional.empty();
    }

    public AuctionStatus status() {
        if (bid.isPresent() && bid.get().amount().compareTo(auctionItem.reserve()) > 0) {
            return SUCCESS;
        }
        return FAILURE;
    }

    public Optional<BigDecimal> soldPrice() {
        if (status().equals(SUCCESS)) {
            return Optional.of(bid.get().amount());
        }
        return Optional.empty();
    }

    public Optional<Participant> soldTo() {
        if (status().equals(SUCCESS)) {
            return Optional.of(bid.get().participant());
        }
        return Optional.empty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuctionResult that = (AuctionResult) o;

        return Objects.equals(auctionItem, that.auctionItem) && Objects.equals(bid, that.bid);

    }

    @Override
    public int hashCode() {
        int result = auctionItem != null ? auctionItem.hashCode() : 0;
        result = 31 * result + (bid != null ? bid.hashCode() : 0);
        return result;
    }
}

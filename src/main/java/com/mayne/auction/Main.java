package com.mayne.auction;

import com.google.common.collect.Lists;
import com.mayne.auction.auctioneer.Auctioneer;
import com.mayne.auction.auctioneer.TimeBasedAuctioneerStrategy;
import com.mayne.auction.participant.BidResult;
import com.mayne.auction.participant.Participant;
import com.mayne.auction.shared.AuctionItem;
import com.mayne.auction.shared.AuctionResult;
import com.mayne.auction.shared.AuctionService;
import com.mayne.auction.shared.AuctionStatus;

import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mayne.auction.shared.AuctionStatus.FAILURE;
import static com.mayne.util.PriceHelper.price;

public class Main {

    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());


    public static void main(String[] args) throws InterruptedException {

        AuctionService auctionService = new AuctionService();

        Participant bidder1 = new Participant(auctionService);

        Participant bidder2 = new Participant(auctionService);

        Deque<Participant> participants = determineBiddingOrder(bidder1, bidder2);

        Auctioneer auctioneer = new Auctioneer(auctionService, new TimeBasedAuctioneerStrategy(5L, 5L));

        AuctionItem auctionItem = new AuctionItem(price("100.0"));

        auctioneer.add(auctionItem);

        auctioneer.start(auctionItem);

        simulateBidding(participants, auctioneer, auctionItem);

        printWinners(auctioneer, auctionItem);
    }

    private static void simulateBidding(Deque<Participant> participants, Auctioneer auctioneer, AuctionItem auctionItem) throws InterruptedException {
        Random random = new Random();
        BigDecimal lastBid = price(random.nextInt(50));

        while (auctioneer.isRunning() && !participants.isEmpty()) {

            Thread.sleep(random.nextInt(500));
            lastBid = lastBid.add(price(random.nextInt(20)));

            Participant next = participants.pop();
            BidResult bidResult = next.bid(auctionItem, lastBid);

            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.log(Level.INFO, String.format("[%s] just %s bid $%s", next.identifier(), buildMessage(bidResult), lastBid.doubleValue()));
            }
        }
    }

    private static void printWinners(Auctioneer auctioneer, AuctionItem auctionItem) {
        Optional<AuctionResult> result = auctioneer.result(auctionItem);

        if (result.isPresent()) {
            AuctionStatus status = result.get().status();
            if (status.equals(FAILURE)) {
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, "No winners!");
                }
            } else {
                Participant participant = result.get().soldTo().get();
                BigDecimal soldPrice = result.get().soldPrice().get();
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, String.format("Auction results are in! [%s] won the auction for $%s!", participant.identifier(), soldPrice.doubleValue()));
                }
            }
        } else {
            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.log(Level.INFO, String.format("Auction was not run for [%s]", auctionItem.identifier()));
            }
        }
    }

    private static String buildMessage(BidResult bidResult) {
        if (bidResult.equals(BidResult.SUCCESS)) {
            return "successfully";
        } else {
            return "unsuccessfully";
        }
    }

    private static Deque<Participant> determineBiddingOrder(Participant bidder1, Participant bidder2) {
        List<Participant> biddingOrder = new ArrayList<>();
        biddingOrder.addAll(Collections.nCopies(10, bidder1));
        biddingOrder.addAll(Collections.nCopies(10, bidder2));
        Collections.shuffle(biddingOrder);
        return Lists.newLinkedList(biddingOrder);
    }
}

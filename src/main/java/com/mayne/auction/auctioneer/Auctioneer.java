package com.mayne.auction.auctioneer;

import com.google.common.util.concurrent.AbstractExecutionThreadService;
import com.mayne.auction.shared.AuctionItem;
import com.mayne.auction.shared.AuctionResult;
import com.mayne.auction.shared.AuctionService;

import java.util.Optional;

public class Auctioneer extends AbstractExecutionThreadService {

    private final AuctionService auctionService;
    private final AuctioneerStrategy auctioneerStrategy;
    private Optional<AuctionItem> currentItem;

    public Auctioneer(AuctionService auctionService, AuctioneerStrategy auctioneerStrategy) {
        this.auctionService = auctionService;
        this.auctioneerStrategy = auctioneerStrategy;
    }

    public void add(AuctionItem auctionItem) throws IllegalStateException {
        auctionService.add(auctionItem);
        currentItem = Optional.of(auctionItem);
    }

    public void start(AuctionItem auctionItem) throws IllegalStateException {
        if(isRunning()) {
            throw new IllegalStateException("Can only run one auction at a time");
        }
        auctionService.start(auctionItem);
        startAsync().awaitRunning();
    }

    public Optional<AuctionResult> result(AuctionItem auctionItem) {
        return auctionService.result(auctionItem);
    }

    @Override
    protected void run() throws Exception {
        while (isRunning() && currentItem.isPresent()) {
            if(auctioneerStrategy.shouldEnd(auctionService, currentItem.get())) {
                currentItem = Optional.empty();
                return;
            }
        }
    }

    public AuctionResult end(AuctionItem auctionItem) {
        return auctionService.end(auctionItem);
    }
}

package com.mayne.auction.auctioneer;

import com.mayne.auction.shared.AuctionItem;
import com.mayne.auction.shared.AuctionService;

public interface AuctioneerStrategy {

    public boolean shouldEnd(AuctionService auctionService, AuctionItem item);
}

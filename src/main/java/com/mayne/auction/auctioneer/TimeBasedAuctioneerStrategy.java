package com.mayne.auction.auctioneer;

import com.mayne.auction.shared.AuctionItem;
import com.mayne.auction.shared.AuctionService;

import java.util.concurrent.TimeUnit;

public class TimeBasedAuctioneerStrategy implements AuctioneerStrategy {

    private final Long timestamp = System.currentTimeMillis();

    private final Long initialDelay;
    private final Long maxWait;

    public TimeBasedAuctioneerStrategy(Long initialDelayInSeconds, Long maxWaitInSeconds) {
        this.initialDelay = TimeUnit.SECONDS.toMillis(initialDelayInSeconds);
        this.maxWait = TimeUnit.SECONDS.toMillis(maxWaitInSeconds);
    }

    @Override
    public boolean shouldEnd(AuctionService auctionService, AuctionItem item) {
        Long currentTime = System.currentTimeMillis();
        Long minimumThreshold = timestamp + initialDelay;
        Long maximumThreshold = minimumThreshold + maxWait;

        if (currentTime < minimumThreshold) {
            return false;
        } else if (currentTime < maximumThreshold && !auctionService.highestBid(item).isPresent()) {
            return false;
        }

        return true;
    }
}

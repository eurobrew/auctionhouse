package com.mayne.auction.participant;

import java.math.BigDecimal;

public class Bid implements Comparable<Bid> {

    private final Participant participant;
    private final BigDecimal amount;

    public Bid(Participant participant, BigDecimal amount) {
        this.participant = participant;
        this.amount = amount;
    }

    public Participant participant() {
        return participant;
    }

    public BigDecimal amount() {
        return amount;
    }

    @Override
    public int compareTo(Bid bid) {
        return amount.compareTo(bid.amount());
    }
}

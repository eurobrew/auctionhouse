package com.mayne.auction.participant;

import com.mayne.auction.shared.AuctionService;
import com.mayne.auction.shared.AuctionResult;
import com.mayne.auction.shared.Item;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

public class Participant {

    private final AuctionService auctionService;
    private final UUID identifier = UUID.randomUUID();

    public Participant(AuctionService auctionService) {
        this.auctionService = auctionService;
    }

    public BidResult bid(Item item, BigDecimal amount) {
        return auctionService.bid(item, new Bid(this, amount));
    }

    public Optional<AuctionResult> result(Item itemToBid) {
        return auctionService.result(itemToBid);
    }

    public UUID identifier() {
        return identifier;
    }
}

package com.mayne.auction.participant;

public enum BidResult {
    BID_TOO_LOW, RESERVE_NOT_MET, SUCCESS
}

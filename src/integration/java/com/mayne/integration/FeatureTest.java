package com.mayne.integration;

import com.mayne.auction.shared.AuctionItem;
import com.mayne.auction.shared.AuctionService;
import com.mayne.auction.auctioneer.Auctioneer;
import com.mayne.auction.auctioneer.AuctioneerStrategy;
import com.mayne.auction.participant.BidResult;
import com.mayne.auction.participant.Participant;
import com.mayne.auction.shared.AuctionResult;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static com.mayne.auction.shared.AuctionStatus.FAILURE;
import static com.mayne.auction.shared.AuctionStatus.SUCCESS;
import static com.mayne.util.PriceHelper.price;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;

public class FeatureTest {

    private static final BigDecimal RESERVE_PRICE = price("22.0");
    private static final BigDecimal LOW_BID_PRICE = price("20.0");
    private static final BigDecimal HIGH_BID_PRICE = price("23.0");

    private AuctionService auctionService = new AuctionService();
    private TestOnlyAuctioneerStrategy auctioneerStrategy;

    @Before
    public void setUp() throws Exception {
        auctionService.startAsync().awaitRunning();
        auctioneerStrategy = new TestOnlyAuctioneerStrategy();
    }

    @After
    public void tearDown() throws Exception {
        auctionService.stopAsync().awaitTerminated();
    }

    @Test
    public void testSupportsBasicAuction() throws Exception {
        Auctioneer auctioneer = new Auctioneer(auctionService, auctioneerStrategy);
        Participant participant1 = new Participant(auctionService);
        Participant participant2 = new Participant(auctionService);

        AuctionItem auctionItem = new AuctionItem(RESERVE_PRICE);

        auctioneer.add(auctionItem);

        auctioneer.start(auctionItem);

        assertThat(participant1.bid(auctionItem, LOW_BID_PRICE), equalTo(BidResult.RESERVE_NOT_MET));
        assertThat(participant2.bid(auctionItem, HIGH_BID_PRICE), equalTo(BidResult.SUCCESS));

        auctioneerStrategy.end();

        Optional<AuctionResult> auctioneerResult = auctioneer.result(auctionItem);

        assertThat(auctioneerResult, equalTo(participant2.result(auctionItem)));

        assertThat(auctioneerResult.get().status(), equalTo(SUCCESS));

        assertThat(auctioneerResult.get().soldPrice().get(), equalTo(HIGH_BID_PRICE));

        assertThat(auctioneerResult.get().soldTo().get(), equalTo(participant2));
    }

    @Test
    public void testSupportsBasicAuctionNotSold() throws Exception {
        Auctioneer auctioneer = new Auctioneer(auctionService, auctioneerStrategy);

        Participant participant = new Participant(auctionService);

        AuctionItem auctionItem = new AuctionItem(RESERVE_PRICE);

        auctioneer.add(auctionItem);

        auctioneer.start(auctionItem);

        assertThat(participant.bid(auctionItem, LOW_BID_PRICE), equalTo(BidResult.RESERVE_NOT_MET));

        auctioneerStrategy.end();

        Optional<AuctionResult> auctioneerResult = auctioneer.result(auctionItem);

        assertThat(auctioneerResult, equalTo(participant.result(auctionItem)));

        assertThat(auctioneerResult.get().status(), equalTo(FAILURE));

        assertFalse(auctioneerResult.get().soldPrice().isPresent());
        assertFalse(auctioneerResult.get().soldTo().isPresent());
    }

    private class TestOnlyAuctioneerStrategy implements AuctioneerStrategy {

        private boolean ended = false;

        @Override
        public boolean shouldEnd(AuctionService auctionService, AuctionItem item) {
            return ended;
        }

        public void end() {
            ended = true;
        }
    }
}

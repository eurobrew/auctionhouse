package com.mayne.auction.auctioneer;

import com.mayne.auction.shared.AuctionItem;
import com.mayne.auction.shared.AuctionService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuctioneerTest {

    @Mock
    private AuctionService auctionService;

    @Mock
    private AuctioneerStrategy auctioneerStrategy;

    @Mock
    private AuctionItem auctionItem;

    @Mock
    private AuctionItem auctionItem2;

    private Auctioneer auctioneer;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        auctioneer = new Auctioneer(auctionService, auctioneerStrategy);
    }

    @Test
    public void testAdd() throws Exception {
        auctioneer.add(auctionItem);
        verify(auctionService).add(same(auctionItem));
    }

    @Test
    public void testRun() throws Exception {
        when(auctioneerStrategy.shouldEnd(auctionService, auctionItem)).thenReturn(true);

        auctioneer.add(auctionItem);
        auctioneer.start(auctionItem);

        verify(auctionService).start(auctionItem);

        while (auctioneer.isRunning()) {
            Thread.sleep(100L);
        }
    }

    @Test
    public void testCannotRunMultipleAuctions() throws Exception {
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("Can only run one auction at a time");

        auctioneer.add(auctionItem);
        auctioneer.add(auctionItem2);
        auctioneer.start(auctionItem);
        auctioneer.start(auctionItem2);

    }
}
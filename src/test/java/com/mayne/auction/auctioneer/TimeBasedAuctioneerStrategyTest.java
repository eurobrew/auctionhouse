package com.mayne.auction.auctioneer;


import com.mayne.auction.shared.AuctionItem;
import com.mayne.auction.shared.AuctionService;
import com.mayne.auction.participant.Bid;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TimeBasedAuctioneerStrategyTest {

    @Mock
    private AuctionService auctionService;

    @Mock
    private AuctionItem auctionItem;

    @Mock
    private Bid bid;

    @Test
    public void testEndsAuctionWithNoBidsAfterTime() throws Exception {
        TimeBasedAuctioneerStrategy timeBasedAuctioneerStrategy = new TimeBasedAuctioneerStrategy(1L, 0L);
        when(auctionService.highestBid(auctionItem)).thenReturn(Optional.<Bid>empty());

        assertFalse(timeBasedAuctioneerStrategy.shouldEnd(auctionService, auctionItem));

        Thread.sleep(1200L);

        assertTrue(timeBasedAuctioneerStrategy.shouldEnd(auctionService, auctionItem));
    }

    @Test
    public void testEndsAuctionWithBidsAfterInitialDelay() throws Exception {
        TimeBasedAuctioneerStrategy timeBasedAuctioneerStrategy = new TimeBasedAuctioneerStrategy(1L, 2L);
        when(auctionService.highestBid(auctionItem)).thenReturn(Optional.of(bid));

        assertFalse(timeBasedAuctioneerStrategy.shouldEnd(auctionService, auctionItem));

        Thread.sleep(1200L);

        assertTrue(timeBasedAuctioneerStrategy.shouldEnd(auctionService, auctionItem));
    }
}
package com.mayne.auction.shared;

import com.mayne.auction.participant.Bid;
import com.mayne.auction.participant.Participant;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.mayne.auction.participant.BidResult.*;
import static com.mayne.util.PriceHelper.price;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class AuctionServiceTest {

    private static final AuctionItem AUCTION_ITEM = new AuctionItem(price("10.0"));

    @Mock
    private Participant participant;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testAdd() throws Exception {
        AuctionService auctionService = new AuctionService();

        assertTrue(auctionService.items().isEmpty());

        auctionService.add(AUCTION_ITEM);

        assertTrue(auctionService.items().contains(AUCTION_ITEM));

        assertThat(auctionService.items().size(), equalTo(1));
    }

    @Test
    public void testCannotAddSameItem() throws Exception {
        thrown.expect(IllegalStateException.class);
        thrown.expectMessage("Cannot add the same item more than once");

        AuctionService auctionService = new AuctionService();

        assertTrue(auctionService.items().isEmpty());

        auctionService.add(AUCTION_ITEM);

        auctionService.add(AUCTION_ITEM);
    }

    @Test
    public void testCannotStartBeforeAdding() throws Exception {
        thrown.expect(IllegalStateException.class);
        thrown.expectMessage("Unknown item");

        AuctionService auctionService = new AuctionService();

        auctionService.start(AUCTION_ITEM);
    }

    @Test
    public void testCannotStartMultipleTimes() throws Exception {
        thrown.expect(IllegalStateException.class);
        thrown.expectMessage("Auction already started");

        AuctionService auctionService = new AuctionService();

        auctionService.add(AUCTION_ITEM);

        auctionService.start(AUCTION_ITEM);
        auctionService.start(AUCTION_ITEM);
    }

    @Test
    public void testCannotAuctionTheSameItemTwice() throws Exception {
        thrown.expect(IllegalStateException.class);
        thrown.expectMessage("Item has already been sold at auction");

        AuctionService auctionService = new AuctionService();

        auctionService.add(AUCTION_ITEM);

        auctionService.start(AUCTION_ITEM);

        auctionService.bid(AUCTION_ITEM, new Bid(participant, price("11.0")));

        auctionService.end(AUCTION_ITEM);

        auctionService.start(AUCTION_ITEM);
    }

    @Test
    public void testAuctionWithBids() throws Exception {
        Bid bid1 = new Bid(participant, price("11.0"));
        Bid bid2 = new Bid(participant, price("12.0"));

        AuctionService auctionService = basicAuction();

        assertThat(auctionService.bid(AUCTION_ITEM, bid1), equalTo(SUCCESS));
        assertThat(auctionService.bid(AUCTION_ITEM, bid2), equalTo(SUCCESS));

        assertThat(auctionService.highestBid(AUCTION_ITEM).get(), equalTo(bid2));

        auctionService.end(AUCTION_ITEM);

        assertTrue(auctionService.currentItems().isEmpty());

        assertThat(auctionService.highestBid(AUCTION_ITEM).get(), equalTo(bid2));
    }

    @Test
    public void testAuctionWithLowerBids() throws Exception {
        Bid bid1 = new Bid(participant, price("9.0"));
        Bid bid2 = new Bid(participant, price("10.0"));
        Bid bid3 = new Bid(participant, price("11.0"));

        AuctionService auctionService = basicAuction();

        assertThat(auctionService.bid(AUCTION_ITEM, bid1), equalTo(RESERVE_NOT_MET));
        assertThat(auctionService.bids().size(), equalTo(1));

        assertThat(auctionService.bid(AUCTION_ITEM, bid3), equalTo(SUCCESS));
        assertThat(auctionService.bids().size(), equalTo(2));

        assertThat(auctionService.bid(AUCTION_ITEM, bid2), equalTo(BID_TOO_LOW));
        assertThat(auctionService.bids().size(), equalTo(2));

        assertThat(auctionService.highestBid(AUCTION_ITEM).get(), equalTo(bid3));

        auctionService.end(AUCTION_ITEM);

        assertTrue(auctionService.currentItems().isEmpty());

        assertThat(auctionService.highestBid(AUCTION_ITEM).get(), equalTo(bid3));
    }

    @Test
    public void testEndWithNoBids() throws Exception {
        AuctionService auctionService = basicAuction();

        auctionService.end(AUCTION_ITEM);

        assertTrue(auctionService.currentItems().isEmpty());
    }

    private static AuctionService basicAuction() {
        AuctionService auctionService = new AuctionService();

        auctionService.add(AUCTION_ITEM);

        auctionService.start(AUCTION_ITEM);

        assertTrue(auctionService.items().contains(AUCTION_ITEM));
        assertThat(auctionService.items().size(), equalTo(1));

        assertTrue(auctionService.currentItems().contains(AUCTION_ITEM.identifier()));
        assertThat(auctionService.currentItems().size(), equalTo(1));

        return auctionService;
    }
}
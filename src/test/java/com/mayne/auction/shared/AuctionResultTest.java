package com.mayne.auction.shared;

import com.mayne.auction.participant.Bid;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.mayne.auction.shared.AuctionStatus.FAILURE;
import static com.mayne.auction.shared.AuctionStatus.SUCCESS;
import static com.mayne.util.PriceHelper.price;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuctionResultTest {

    @Mock
    private Bid bid;

    @Mock
    private AuctionItem auctionItem;

    @Test
    public void testSuccess() throws Exception {
        when(bid.amount()).thenReturn(price("11.00"));
        when(auctionItem.reserve()).thenReturn(price("10.00"));

        AuctionResult auctionResult = new AuctionResult(auctionItem, bid);

        assertThat(auctionResult.status(), equalTo(SUCCESS));
    }


    @Test
    public void testFailure() throws Exception {
        when(bid.amount()).thenReturn(price("1.00"));
        when(auctionItem.reserve()).thenReturn(price("10.00"));

        AuctionResult auctionResult = new AuctionResult(auctionItem, bid);

        assertThat(auctionResult.status(), equalTo(FAILURE));

        assertFalse(auctionResult.soldPrice().isPresent());
        assertFalse(auctionResult.soldTo().isPresent());
    }
}
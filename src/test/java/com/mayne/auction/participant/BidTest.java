package com.mayne.auction.participant;

import com.google.common.collect.Lists;
import com.mayne.auction.participant.Bid;
import com.mayne.auction.participant.Participant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static com.mayne.util.PriceHelper.price;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class BidTest {

    @Mock
    private Participant participant;

    @Test
    public void testComparable() throws Exception {
        Bid bid1 = new Bid(participant, price("10.0"));
        Bid bid2 = new Bid(participant, price("11.0"));
        Bid bid3 = new Bid(participant, price("9.0"));
        List<Bid> bids = Lists.newArrayList(bid1, bid2, bid3);

        Collections.sort(bids);

        assertThat(bids.get(0).amount(), equalTo(bid3.amount()));
        assertThat(bids.get(1).amount(), equalTo(bid1.amount()));
        assertThat(bids.get(2).amount(), equalTo(bid2.amount()));
    }
}
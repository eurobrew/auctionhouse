# Auction House

## Build
* To package into a jar, run `gradlew jar`, which will be put into `build/libs`. Or,
* To run the full test suite, run `gradlew test integration jar --info`

## Run
* To run, `java -jar auctionHouse.jar`, or
* From IntelliJ, run the `Main` Run configuration or the `Auction House Unit/Integration` Run configs